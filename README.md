How to setup VetSoft
Vetsoft is a Laravel based veternary managment software suite that meets parity with Ezyvet products.

Requires PHP 8.1 +

To get started, clone the repository and checkout the "primary", branch and not master.

Set the public folder as the web root folder

Install standard laravel 8.0 dependencies

sudo apt install php libapache2-mod-php php-mbstring php-cli php-bcmath php-json php-xml php-zip php-pdo php-common php-tokenizer php-mysql

Move the .env.sample file to .env and set up an empty database

Run the migrations with "php artisan migrate"

Install the composer dependencies with "composer install"

Set up a user with "php artisan nova:user"

Seed the database optionally with medical procedure's and medical procedure categories with

php artisan db:seed --class=MedicalProcedureCategorySeeder php artisan db:seed --class=MedicalProcedureSeeder

Additionally this product has an inventory management system which is not shown but can be enabled by visiting app/Providers/NovaServiceProvider.php

If you need assistance with this repository feel free to reach out to me at justchristopher123@gmail.com